#include "SodukuHelper.h"
#include <stdio.h>


SodukuHelper::SodukuHelper(void)
{
}


SodukuHelper::~SodukuHelper(void)
{
}

void SodukuHelper::inputBoard()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			scanf("%d",&board[i][j]);
		}
	}
}

void SodukuHelper::initialBoard()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			iNewState[i][j] = 0;
			iColState[i][j] = 9;
			iRowState[i][j] = 9;
			iBlockState[i][j] = 9;
			iNumberCount[i][j] = 9;
		}
	}
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			for(int k = 0 ;k < 9 ;k++)
			{
				bCanBeUse[i][j][k] = true;
			}
		}
	}
}

void SodukuHelper::setTruthAll()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			if(board[i][j] != 0/* && iNewState[i][j] == 1*/)
			{
				setTruth(i,j,board[i][j] - 1);
				/*iNewState[i][j] = 1; //当前位置设置为新的确定值的位置
				iNumberCount[i][j] = 0; //将当前位置可用的数字设置为0，即无任何数字可用

				clearCanBeUse(i,j);
				iColState[j][board[i][j] - 1] = 0; //将当前列对应当前格子的值设置为0，即不可用
				iRowState[i][board[i][j] - 1] = 0; //将当前行对应当前格子的值设置为0，即不可用
				int iBlockNumber = i / 3 * 3 + j / 3;
				iBlockState[iBlockNumber][board[i][j] - 1] = 0;//将当前格子所在块对应当前格子的值设置为0，即不可用*/
				
			}
		}
	}
}

void SodukuHelper::setTruth(int row,int col,int index)
{
	iNumberCount[row][col] = 0;//将当前位置可用的数字设置为0，即无任何数字可用
	//iNewState[row][col] = 1;//当前位置设置为新的确定值的位置
	//state = true;
	clearCanBeUse(row,col);
	iColState[col][index] = 0;
	iRowState[row][index] = 0;
	int iBlockNumber = row / 3 * 3 + col / 3;
	iBlockState[iBlockNumber][index] = 0;
}

void SodukuHelper::preCal()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			if(board[i][j] != 0/* && iNewState[i][j] == 1*/)
			{
				for(int k = 0 ;k < 9 ;k++)
				{
					if(iRowState[i][k] != 0)
						iRowState[i][k]--;
					if(iColState[j][k] != 0)
						iColState[j][k]--;
					int iBlockNumber = i / 3 * 3 + j / 3;
					if(iBlockState[iBlockNumber][k] != 0)
						iBlockState[iBlockNumber][k]--;
				}
			}
		}
	}
}




void SodukuHelper::calBoard()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			if(0 != board[i][j]/*1 == iNewState[i][j]*/)
			{
				setColFalse(j,board[i][j] - 1);
				setRowFalse(i,board[i][j] - 1);
				int iBlockNumber = i / 3 * 3 + j / 3;
				setBlockFalse(iBlockNumber,board[i][j] - 1);
				iNewState[i][j] = -1;
			}
		}
	}
}

void SodukuHelper::setColFalse(int col,int index)
{
	for(int i = 0 ;i < 9 ;i++)
	{
		if(bCanBeUse[i][col][index])
		{
			bCanBeUse[i][col][index] = false;
			setMinusMore(i,col,index);
		}
	}
}


void SodukuHelper::setRowFalse(int row,int index)
{
	for(int i = 0 ;i < 9 ;i++)
	{
		if(bCanBeUse[row][i][index])
		{
			bCanBeUse[row][i][index] = false;
			setMinusMore(row,i,index);

		}
	}
}
void SodukuHelper::setBlockFalse(int block,int index)
{
	int iBaseCol = (block % 3) * 3;
	int iBaseRow = block / 3 * 3;
	
	for(int i = 0 ;i < 3 ;i++)
	{
		for(int j = 0 ;j < 3 ;j++)
		{
			if(bCanBeUse[iBaseRow + i][iBaseCol + j][index])
			{
				bCanBeUse[iBaseRow + i][iBaseCol + j][index] = false;

				setMinusMore(iBaseRow + i,iBaseCol + j,index);
			}
		}
	}
}

bool SodukuHelper::calNewBoard()
{
	bool state = false;
	for(int i = 0 ;i < 9 ;i++)
	{
		for(int j = 0 ;j < 9 ;j++)
		{
			if(iNumberCount[i][j] == 1 && iNewState[i][j] != -1)
			{
				for(int k = 0 ;k < 9 ;k++)
				{
					if(bCanBeUse[i][j][k])
					{
						state = true;
						board[i][j] = k + 1;
						iNewState[i][j] = 1; //当前位置设置为新的确定值的位置
						//setTruth(i,j,k);
					}
				}
			}
			else
			{
				for(int k = 0 ;k < 9 ;k++)
				{
					if(bCanBeUse[i][j][k])
					{
						int iBlockNumber = i / 3 * 3 + j / 3;
						if(iRowState[i][k] == 1 || iColState[j][k] == 1 || iBlockState[iBlockNumber][k] == 1)
						{
							state = true;
							board[i][j] = k + 1;
							iNewState[i][j] = 1; //当前位置设置为新的确定值的位置
							//setTruth(i,j,k);
							break;
						}
					}
				}
			}
		}
	}
	return state;
}


void SodukuHelper::cal()
{
	bool state = true;
	while(state)
	{
		showBoard();
		initialBoard();
		setTruthAll();
		preCal();
		calBoard();
		state = calNewBoard();
	}
}

void SodukuHelper::showBoard()
{
	for(int i = 0 ;i < 9 ;i++)
	{
		if(i % 3 == 0)
			printf("\n");
		for(int j = 0 ;j < 9 ;j++)
		{
			printf("%d ",board[i][j]);
			if(j % 3 == 2)
				printf(" ");
		}
		printf("\n");
	}
	for(int i = 0 ;i < 20 ;i++)
	{
		printf("-");
	}
	printf("\n");
}


void SodukuHelper::work()
{
	this->inputBoard();
	this->cal();
}


void SodukuHelper::setMinus(int row,int col,int index)
{
	int iBlockNumber = row / 3 * 3 + col / 3;
	if(iRowState[row][index] != 0)
		iRowState[row][index]--;
	if(iColState[col][index] != 0)
		iColState[col][index]--;
	if(iBlockState[iBlockNumber][index] != 0)
		iBlockState[iBlockNumber][index]--;
}
void SodukuHelper::setMinusMore(int row,int col,int index)
{
	if(iNumberCount[row][col] != 0)
		iNumberCount[row][col]--;
	setMinus(row,col,index);
}

void SodukuHelper::clearCanBeUse(int row,int col)
{
	for(int k = 0 ;k < 9 ;k++)
	{
		bCanBeUse[row][col][k] = false;
	}
}