#pragma once
class SodukuHelper
{
public:
	SodukuHelper(void);
	~SodukuHelper(void);
	void work();
private:
	int board[9][9];
	int iColState[9][9];
	int iRowState[9][9];
	int iBlockState[9][9];

	int iNewState[9][9];

	bool bCanBeUse[9][9][9];
	int iNumberCount[9][9];

	void preCal();
	void setTruthAll();
	void cal();
	void calBoard();
	bool calNewBoard();

	void inputBoard();
	void initialBoard();
	void showBoard();
	void judgeByRule();

	void setMinusMore(int row,int col,int index);
	void setMinus(int row,int col,int index);

	void setTruth(int row,int col,int index);
	void updateCanBeUse();

	void clearCanBeUse(int row,int col);


	void setColFalse(int col,int index);
	void setRowFalse(int row,int index);
	void setBlockFalse(int block,int index);
};


/*
block˳��
0 1 2
3 4 5
6 7 8
*/
